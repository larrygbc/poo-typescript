import {Persona} from "./persona";
export class Paciente extends Persona{
	//atributo
	private Sexo: string;
	//constructor
	constructor(n:number, no:string, f:Date, s:string){
		super(n, no, f);
		this.Sexo = s;
	}
	//get
	public getSexo() : string {
		return this.Sexo;
	}
	//set
	public setSexo(v : string) {
		this.Sexo = v;
	}
}