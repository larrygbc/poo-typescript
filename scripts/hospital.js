define(["require", "exports", "./medico", "./paciente", "./ingreso"], function (require, exports, medico_1, paciente_1, ingreso_1) {
    "use strict";
    exports.__esModule = true;
    var Hospital = /** @class */ (function () {
        function Hospital() {
            this.codIngreso = 1;
            this.medicos = [];
            this.pacientes = [];
            this.ingresos = [];
        }
        Hospital.prototype.getcodIngreso = function () {
            return this.codIngreso++;
        };
        Hospital.prototype.NuevoPaciente = function (i, y, f, x) {
            var paciente = new paciente_1.Paciente(i, y, f, x);
            this.pacientes.push(paciente);
        };
        Hospital.prototype.NuevoIngreso = function (c, f, s, d, e, p) {
            var ingreso = new ingreso_1.Ingreso(c, f, s, d, e, p);
            this.ingresos.push(ingreso);
        };
        Hospital.prototype.paciente = function () {
            alert(this.pacientes.length);
        };
        return Hospital;
    }());
    //objeto de la clase hospital
    var hospital = new Hospital();
    //medicos
    var fec_med1 = new Date(1980, 11, 3);
    var Nuevo_medico1 = new medico_1.Medico(1000, "Enzo Bonilla", fec_med1, 1111);
    hospital.medicos.push(Nuevo_medico1);
    var fec_med2 = new Date(1970, 1, 13);
    var Nuevo_medico2 = new medico_1.Medico(1001, "Mathias Bonilla", fec_med2, 1112);
    hospital.medicos.push(Nuevo_medico2);
    var fec_med3 = new Date(1975, 6, 23);
    var Nuevo_medico3 = new medico_1.Medico(1002, "Carlos Perez", fec_med3, 1113);
    hospital.medicos.push(Nuevo_medico3);
    var fec_med4 = new Date(1966, 5, 30);
    var Nuevo_medico4 = new medico_1.Medico(1003, "Valentino Florez", fec_med4, 1114);
    hospital.medicos.push(Nuevo_medico4);
    var fec_med5 = new Date(1960, 0, 12);
    var Nuevo_medico5 = new medico_1.Medico(1004, "Monica Varela", fec_med5, 1115);
    hospital.medicos.push(Nuevo_medico5);
    function NuevoPaci() {
        var i = parseInt(prompt("Dime el numero de la seguridad social:"));
        var y = prompt("Dime tu nombre:");
        y = y.toLowerCase();
        var fecha = prompt("Dime la fecha de nacimiento: ");
        var dia = parseInt(fecha.substr(0, fecha.indexOf("/")));
        var mes = parseInt(fecha.substr(fecha.indexOf("/") + 1, fecha.indexOf("/")));
        var ano = parseInt(fecha.substr(-4));
        var f = new Date(ano, mes - 1, dia);
        var x = prompt("Sexo de la persona:");
        hospital.NuevoPaciente(i, y, f, x);
        console.log(hospital.pacientes);
    }
    function Nuevo_Ingreso() {
        var existe = false;
        var numSS = parseInt(prompt("Numero de la seguridad social: "));
        var pos;
        for (var i = 0; i < hospital.pacientes.length; ++i) {
            if (hospital.pacientes[i].getNumSS() == numSS) {
                existe = true;
                pos = i;
            }
        }
        if (existe) {
            var sintoma = [];
            var ok = false;
            while (ok == false) {
                var pregunta = prompt("Algun sintoma:  " + "\nDiga no para terminar.");
                if (pregunta == 'no') {
                    ok = true;
                }
                else {
                    sintoma.push(pregunta);
                }
            }
            var f = new Date();
            var e = "Pendiente";
            hospital.NuevoIngreso(hospital.getcodIngreso(), f, sintoma, null, e, hospital.pacientes[pos]);
        }
        else {
            alert("No existe este paciente");
        }
        console.log(hospital.ingresos);
    }
    function Lista_Pendiente() {
        for (var i = 0; i < hospital.ingresos.length; ++i) {
            if (hospital.ingresos[i].getEstado() == 'Pendiente') {
                var fec_ing = hospital.ingresos[i].getFechaHora();
                //calcular edad
                var fecha = hospital.ingresos[i].getPaciente().getFechaNac();
                var fecha_actual = new Date();
                var anyos = fecha_actual.getTime() - fecha.getTime();
                var anyos = Math.floor(anyos / (1000 * 60 * 60 * 24 * 365));
                alert("Nombre: " + hospital.ingresos[i].getPaciente().getNombre() + "\n" +
                    "Fecha de ingreso: " + fec_ing.getDay() + "/" + fec_ing.getMonth() + "/" + fec_ing.getFullYear() + " " +
                    " Hora de ingreso: " + fec_ing.getHours() + ":" + fec_ing.getMinutes() + ":" + fec_ing.getSeconds() + "\n" +
                    "Sexo: " + hospital.ingresos[i].getPaciente().getSexo() + " \n" +
                    "Edad: " + anyos);
            }
            console.log(hospital.ingresos[i].getEstado() == "Pendiente");
        }
    }
    function AsignarMedico() {
        var existeN = false;
        var PosM;
        var num_col = parseInt(prompt("Numero de colegiado: "));
        for (var j = 0; j < hospital.medicos.length; j++) {
            if (hospital.medicos[j].getNumColegiado() == num_col) {
                existeN = true;
                PosM = j;
            }
        }
        var EstadoP = false;
        var pos;
        var existeC = false;
        if (!existeN) {
            alert("No existe el numero de colegiado.");
        }
        else {
            var codigo = parseInt(prompt("Numero de ingreso: "));
            for (var i = 0; i < hospital.ingresos.length; i++) {
                if (hospital.ingresos[i].getCodigo() == codigo) {
                    existeC = true;
                    if (hospital.ingresos[i].getEstado() == "Pendiente") {
                        EstadoP = true;
                    }
                    pos = i;
                }
            }
        }
        if (!existeC) {
            alert("No existe el codigo de ingreso.");
        }
        else if (!EstadoP) {
            alert("El estado ya no esta en pendiente");
        }
        else if (existeC && existeN && EstadoP) {
            hospital.ingresos[pos].setEstado("En curso");
            hospital.ingresos[pos].setMedico(hospital.medicos[PosM]);
            alert("Estado cambiado");
            console.log(hospital.ingresos);
        }
    }
    function DiagnosticarIngreso() {
        var ok1 = false;
        var existeC = false;
        var pos;
        while (ok1 == false) {
            var codigo = parseInt(prompt("Numero de ingreso: "));
            for (var i = 0; i < hospital.ingresos.length; i++) {
                if (hospital.ingresos[i].getCodigo() == codigo) {
                    ok1 = true;
                    existeC = true;
                    pos = i;
                }
            }
            if (!existeC) {
                alert("No existe el codigo de ingreso.");
            }
        }
        if (existeC) {
            alert("Sintomas: \n" + hospital.ingresos[pos].getSintoma());
            var diagnostico = prompt("Diagnostico del paciente con codigo " + hospital.ingresos[pos].getCodigo());
            hospital.ingresos[pos].setDiagnostico(diagnostico);
        }
        console.log(hospital.ingresos);
    }
    function DarAlta() {
        var ok1 = false;
        var existeC = false;
        var pos;
        while (ok1 == false) {
            var codigo = parseInt(prompt("Numero de ingreso: "));
            for (var i = 0; i < hospital.ingresos.length; i++) {
                if (hospital.ingresos[i].getCodigo() == codigo) {
                    ok1 = true;
                    existeC = true;
                    pos = i;
                }
            }
            if (!existeC) {
                alert("No existe el codigo de ingreso.");
            }
        }
        if (existeC) {
            if (hospital.ingresos[pos].getDiagnostico() == null || hospital.ingresos[pos].getEstado() == "Pendiente") {
                alert("No se puede dar de alta todavia al paciente.");
            }
            else {
                hospital.ingresos[pos].setEstado("Alta");
                alert("Se ha dado de alta a " + hospital.ingresos[pos].getPaciente().getNombre());
            }
        }
        console.log(hospital.ingresos);
    }
    function HistorialPaciente() {
        var existeSS = false;
        var SS = parseInt(prompt("Numero de la seguridad social: "));
        var pos;
        for (var i = 0; i < hospital.ingresos.length; i++) {
            if (hospital.ingresos[i].getPaciente().getNumSS() == SS) {
                pos = i;
                existeSS = true;
            }
        }
        if (!existeSS) {
            alert("No existe el numero de seguridad social dado.");
        }
        else {
            alert("Num. S.S.: " + hospital.ingresos[pos].getPaciente().getNumSS() + "\nNombre: " + hospital.ingresos[pos].getPaciente().getNombre() +
                "\nFecha de nacimiento: " + hospital.ingresos[pos].getPaciente().getFechaNac() + "\nSexo: " + hospital.ingresos[pos].getPaciente().getSexo());
            for (var i = 0; i < hospital.ingresos.length; ++i) {
                if (hospital.ingresos[i].getPaciente().getNumSS() == SS) {
                    alert("historial del paciente: " + hospital.ingresos[i].getPaciente().getNombre() +
                        "\nFecha de ingreso: " + hospital.ingresos[i].getFechaHora() + "\nSintomas: " + hospital.ingresos[i].getSintoma() +
                        "\nDiagnostico: " + hospital.ingresos[i].getDiagnostico());
                }
            }
        }
    }
    function ListadoMedicos() {
        for (var i = 0; i < hospital.medicos.length; i++) {
            alert("Medico No. " + (i + 1) +
                "\nNombre: " + hospital.medicos[i].getNombre() + "\nFecha de nacimiento: " + hospital.medicos[i].getFechaNac() +
                "\nNumero de colegiado: " + hospital.medicos[i].getNumColegiado());
        }
    }
    function BuscarPacientes() {
        var nombre = prompt("Buscar paciente por nombre: ");
        nombre = nombre.toLowerCase();
        console.log(nombre);
        var existe = false;
        var comprobar;
        for (var i = 0; i < hospital.pacientes.length; i++) {
            comprobar = hospital.pacientes[i].getNombre().indexOf(nombre);
            if (comprobar != -1) {
                alert("Nombre: " + hospital.pacientes[i].getNombre() + "\nNum. S.S.: " + hospital.pacientes[i].getNumSS() +
                    "\nFecha de nacimiento: " + hospital.pacientes[i].getFechaNac().getDay() + "/" + hospital.pacientes[i].getFechaNac().getMonth() + "/" +
                    hospital.pacientes[i].getFechaNac().getFullYear() + "\nSexo: " + hospital.pacientes[i].getSexo());
            }
        }
    }
    function total() {
        hospital.paciente();
    }
    document.getElementById("Nuevo_Paciente").onclick = NuevoPaci;
    document.getElementById("Total").onclick = total;
    document.getElementById("Nuevo_Ingreso").onclick = Nuevo_Ingreso;
    document.getElementById("Lista_Pendientes").onclick = Lista_Pendiente;
    document.getElementById("Asignar_Medico").onclick = AsignarMedico;
    document.getElementById("Diagnosticar_Ingreso").onclick = DiagnosticarIngreso;
    document.getElementById("Dar_Alta").onclick = DarAlta;
    document.getElementById("Hisotrial_Paciente").onclick = HistorialPaciente;
    document.getElementById("Listar_Medicos").onclick = ListadoMedicos;
    document.getElementById("Buscar_Pacientes").onclick = BuscarPacientes;
});
