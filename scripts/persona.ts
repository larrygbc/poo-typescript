export class Persona{
	//atributos
	private NumSS: number;
	private Nombre: string;
	private FechaNac: Date;
	//constructor
	constructor(n:number, no:string, f:Date){
		this.NumSS = n;
		this.Nombre = no;
		this.FechaNac = f;
	}
	//get
	public getNumSS() : number {
		return this.NumSS;
	}
	public getNombre() : string {
		return this.Nombre;
	}
	public getFechaNac() : Date {
		return this.FechaNac;
	}
	//set
	public setNumSS(v : number) {
		this.NumSS = v;
	}
	public setNombre(v : string) {
		this.Nombre = v;
	}
	public setFechaNac(v : Date) {
		this.FechaNac = v;
	}

}
