define(["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var Ingreso = /** @class */ (function () {
        //constructor
        function Ingreso(c, f, s, d, e, p) {
            this.Sintoma = [];
            this.Codigo = c;
            this.FechaHora = f;
            this.Sintoma = s;
            this.Diagnostico = d;
            this.Estado = e;
            this.Paciente = p;
            this.Medico = null;
        }
        //get
        Ingreso.prototype.getCodigo = function () {
            return this.Codigo;
        };
        Ingreso.prototype.getFechaHora = function () {
            return this.FechaHora;
        };
        Ingreso.prototype.getSintoma = function () {
            return this.Sintoma;
        };
        Ingreso.prototype.getDiagnostico = function () {
            return this.Diagnostico;
        };
        Ingreso.prototype.getEstado = function () {
            return this.Estado;
        };
        Ingreso.prototype.getMedico = function () {
            return this.Medico;
        };
        Ingreso.prototype.getPaciente = function () {
            return this.Paciente;
        };
        //set
        Ingreso.prototype.setCodigo = function (v) {
            this.Codigo = v;
        };
        Ingreso.prototype.setFechaHora = function (v) {
            this.FechaHora = v;
        };
        Ingreso.prototype.setSintoma = function (v) {
            this.Sintoma = v;
        };
        Ingreso.prototype.setDiagnostico = function (v) {
            this.Diagnostico = v;
        };
        Ingreso.prototype.setEstado = function (v) {
            this.Estado = v;
        };
        Ingreso.prototype.setPaciente = function (p) {
            this.Paciente = p;
        };
        Ingreso.prototype.setMedico = function (m) {
            this.Medico = m;
        };
        return Ingreso;
    }());
    exports.Ingreso = Ingreso;
});
