define(["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
    var Persona = /** @class */ (function () {
        //constructor
        function Persona(n, no, f) {
            this.NumSS = n;
            this.Nombre = no;
            this.FechaNac = f;
        }
        //get
        Persona.prototype.getNumSS = function () {
            return this.NumSS;
        };
        Persona.prototype.getNombre = function () {
            return this.Nombre;
        };
        Persona.prototype.getFechaNac = function () {
            return this.FechaNac;
        };
        //set
        Persona.prototype.setNumSS = function (v) {
            this.NumSS = v;
        };
        Persona.prototype.setNombre = function (v) {
            this.Nombre = v;
        };
        Persona.prototype.setFechaNac = function (v) {
            this.FechaNac = v;
        };
        return Persona;
    }());
    exports.Persona = Persona;
});
