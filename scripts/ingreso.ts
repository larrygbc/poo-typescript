import {Medico} from './medico';
import {Paciente} from './paciente';

export class Ingreso{
	//atributo
	private Codigo: number;
	private FechaHora: Date;
	private Sintoma: Array<string>=[];
	private Diagnostico: string;
	private Estado: string;
	private Medico: Medico;
	private Paciente: Paciente;
	
	//constructor
	constructor(c:number, f:Date, s:Array<string>, d:string, e:string, p:Paciente){
		this.Codigo = c;
		this.FechaHora = f;
		this.Sintoma = s;
		this.Diagnostico = d;
		this.Estado = e;
		this.Paciente = p;
		this.Medico = null;
	}
	
	//get
	public getCodigo() : number {
		return this.Codigo;
	}
	public getFechaHora() : Date {
		return this.FechaHora;
	}
	public getSintoma() : Array<string> {
		return this.Sintoma;
	}
	public getDiagnostico() : string {
		return this.Diagnostico;
	}
	public getEstado() : string {
		return this.Estado;
	}
	public getMedico() :Medico{
		return this.Medico;
	}
	public getPaciente() : Paciente{
		return this.Paciente;
	}
	//set
	public setCodigo(v : number) {
		this.Codigo = v;
	}
	public setFechaHora(v : Date) {
		this.FechaHora = v;
	}
	public setSintoma(v : Array<string>) {
		this.Sintoma = v;
	}
	public setDiagnostico(v : string) {
		this.Diagnostico = v;
	}
	public setEstado(v : string) {
		this.Estado = v;
	}
	public setPaciente(p: Paciente){
		this.Paciente = p;
	}
	public setMedico(m: Medico){
		this.Medico = m;
	}
}