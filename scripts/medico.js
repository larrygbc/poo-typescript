var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "./persona"], function (require, exports, persona_1) {
    "use strict";
    exports.__esModule = true;
    var Medico = /** @class */ (function (_super) {
        __extends(Medico, _super);
        //constructor
        function Medico(n, no, f, nc) {
            var _this = _super.call(this, n, no, f) || this;
            _this.NumColegiado = nc;
            return _this;
        }
        //get
        Medico.prototype.getNumColegiado = function () {
            return this.NumColegiado;
        };
        //set
        Medico.prototype.setNumColegiado = function (v) {
            this.NumColegiado = v;
        };
        return Medico;
    }(persona_1.Persona));
    exports.Medico = Medico;
});
