import {Persona} from "./persona";
export class Medico extends Persona{
	//atributo
	private NumColegiado: number;

	//constructor
	constructor(n:number, no:string, f:Date, nc:number){
		super(n, no, f);
		this.NumColegiado = nc;
	}
	//get
	public getNumColegiado() : number {
		return this.NumColegiado;
	}
	//set
	public setNumColegiado(v : number) {
		this.NumColegiado = v;
	}
}